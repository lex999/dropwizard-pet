package com.service;

import com.service.grpc.GrpcServer;
import com.service.grpc.GrpcServerBuilder;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class KafkaApplication extends Application<AppConfig> {

    public static void main(String[] args) throws Exception {
        new KafkaApplication().run(args);
    }


    @Override
    public String getName() {
        return "Test kafka";
    }

    @Override
    public void initialize(Bootstrap<AppConfig> bootstrap) {
        super.initialize(bootstrap);
    }

    @Override
    public void run(AppConfig appConfig, Environment environment) {
        environment.healthChecks().register("Test health check", new AppHealthCheck());
        environment.jersey().register(new ImageResources(new ImageReadService()));
        environment.lifecycle().manage(new GrpcServer(GrpcServerBuilder.build()));
    }
}
