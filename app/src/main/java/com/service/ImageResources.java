package com.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/images")
@Produces(MediaType.APPLICATION_JSON)
public class ImageResources {

    private final ImageReadService imageReadService;

    public ImageResources(ImageReadService imageReadService) {
        this.imageReadService = imageReadService;
    }

    @GET
    @Path("{id}")
    public ImageInfo getById(@PathParam("id") String id) {
        return imageReadService.get(id);
    }
}
