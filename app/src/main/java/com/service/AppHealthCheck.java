package com.service;

import com.codahale.metrics.health.HealthCheck;

public class AppHealthCheck extends HealthCheck {

    @Override
    protected Result check() {
        //TODO check kafka stream
        return Result.healthy("App is fine)");
    }
}
