package com.service.grpc;


import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GrpcServiceImpl extends ImageReadServiceGrpc.ImageReadServiceImplBase {

    @Override
    public void getImageByReq(ImageService.GetImageRequest request, StreamObserver<ImageService.GetImageResponse> responseObserver) {
      log.debug("Request {}", request);

      ImageService.GetImageResponse response = ImageService.GetImageResponse
              .newBuilder()
              .setId(request.getId())
              .setName("Name" + request.getId())
              .setType("Type" + request.getId())
              .build();

      responseObserver.onNext(response);
      responseObserver.onCompleted();
    }
}
