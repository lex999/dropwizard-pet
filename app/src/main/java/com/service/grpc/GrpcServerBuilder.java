package com.service.grpc;

import io.grpc.Server;
import io.grpc.ServerBuilder;

public class GrpcServerBuilder {

    public static Server build() {
        return ServerBuilder.forPort(8090)
                .addService(new GrpcServiceImpl())
                .build();
    }

}
