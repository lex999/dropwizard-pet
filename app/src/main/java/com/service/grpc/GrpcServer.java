package com.service.grpc;

import io.dropwizard.lifecycle.Managed;
import io.grpc.Server;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Slf4j
public class GrpcServer implements Managed {

    private final Server grpcServer;

    public GrpcServer(Server grpcServer) {
        this.grpcServer = grpcServer;
    }

    @Override
    public void start() throws IOException {
        log.info("About start grpc server");
        grpcServer.start();
        log.info("grpc server started on port {}", grpcServer.getPort());
    }

    @Override
    public void stop() throws Exception {
        log.info("Stopping grpc server on port {}", grpcServer.getPort());
        boolean stoped = grpcServer.shutdown().awaitTermination(2, TimeUnit.MINUTES);

        if (stoped) {
            log.debug("Server stopped without problems");
            return;
        }

        log.debug("Forcefully shutdown");
        grpcServer.shutdownNow();

    }


}
